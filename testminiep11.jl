include("miniep11.jl")
using Test

function testapalindromo()
    println("Começando testes automatizados... ")
    print("Testando função palindromo...   ")
    @test palindromo("") == true
    @test palindromo("Socorram-me, subi no ônibus em Marrocos!") == true
    @test palindromo("ovo") == true
    @test palindromo("MiniEP11") == false
    @test palindromo("A mãe te ama.") == true
    @test palindromo("Passei em MAC0110") == false
    println("OK!")
    println(" ")
    printstyled("Todos os testes obtiveram sucesso!", bold=true, color=:green)

end

testapalindromo()