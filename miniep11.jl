using Unicode

function palindromo(string)
    string = Unicode.normalize(string, stripmark = true)
    string = lowercase(string)
    array_string = string2array(string)
    array = arruma_array(array_string)
    ponteiro2 = length(array)
    for i in 1:(length(array)÷2)
        if array[i] == array[ponteiro2]
            i += 1
            ponteiro2 -= 1
        else
            return false
        end
    end
    return true
end
function arruma_array(array)
    caracteres = ['!','.','-',' ',',']
    for i in 1:length(array)
        for j in 1:length(caracteres)
            if array[i] == caracteres[j]
                array = deleteat!(array, i)
                return arruma_array(array)
            end
        end
    end
    return array
end
function string2array(string)
    res = []
    for i in 1:length(string)
        push!(res, string[i])
        i += 1
    end
    return res
end
