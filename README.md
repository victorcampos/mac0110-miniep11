# MAC0110-MiniEP11

MiniEP11 coursework for MAC0110

This miniEP11 coursework is based on writing a julia function
capable of telling whether a given string is a palindrome.
This string is not case sensitive and can include punctuation
and space characters without loss of functionality.

This code will return a boolean true or false depending on
the input string being able to form a palindrome, or not.

For usage, please run the given automated tests or modify 
them to your liking.
